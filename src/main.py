import numpy as np


def enumfilter(m):
    return map(lambda t: t[0], filter(lambda t: t[1] == 1, np.ndenumerate(m)))

def tile_color_index(i):
    return (i[0] + i[1]) % 2


def tile_index_lookup(m):
    lookups = [{}, {}]
    js = [0, 0]

    for i in enumfilter(m):
        cindex = tile_color_index(i)
        lookups[cindex][i] = js[cindex]
        js[cindex] += 1

    return lookups, js


def matrix_entry(i, j):
    if i[0] == j[0] and abs(i[1] - j[1]) == 1:
        return 1.0
    elif i[1] == j[1] and abs(i[0] - j[0]) == 1:
        return 1.0j
    else:
        return 0.0


def get_determinant(m):
    tile_index_lookups, tile_color_counts = tile_index_lookup(m)

    matrix = np.full(shape=tuple(tile_color_counts), fill_value=0, dtype="complex")
    for i in enumfilter(m):
        if tile_color_index(i) != 0:
            continue
        for j in enumfilter(m):
            if tile_color_index(j) != 1:
                continue
            iindex = tile_index_lookups[0][i]
            jindex = tile_index_lookups[1][j]
            matrix[iindex, jindex] = matrix_entry(i, j)

    return np.linalg.det(matrix)


if __name__ == "__main__":
    # sunglasses
    #m = np.array([
    #    [1, 1, 1, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1],
    #    [1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1],
    #    [0, 0, 0, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 0, 0, 0],
    #    [0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0]], dtype="int")

    #det = round(get_determinant(m))
    #abs = np.abs(det)
    #print(f"{det=} {abs=}")

    # 2 by n
    for n in range(10):
        m = np.array([
            [1 for _ in range(n)],
            [1 for _ in range(n)]], dtype="int")

        det = round(np.abs(get_determinant(m)))
        print(f"{n=}, {det=}")
